const express = require('express');
const bunyan = require('bunyan');
const {LoggingBunyan} = require('@google-cloud/logging-bunyan');
const cloudLogger = new LoggingBunyan();

// logs come in levels
// trace
// debug
// info
// warn
// error
// fatal
const app = express();
const config = {
    name:'signup-app',
    // steams are whre you log the data
    streams:[
        {stream:process.stdout, level:'info'},
        {path:'signups.log', type:'file'},
        cloudLogger.stream('info')
    ]
}
const logger = bunyan.createLogger(config)

app.get('/signup/:email', (req, res)=>{

    const email = req.params.email;
    

    if(email.includes("@")){
        logger.info('A new person registered with email '+ email);
        res.send('Your email ' +email + ' is being processed');
    }else{
        logger.warn('A person tried to use an invalid email ' + email)
        res.status(422);
        res.send('That is an inappropirate email')
    }

});

app.listen(3000,()=>console.log('app started'));